import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ProcessingRateService} from './services/processing-rate.service';
import {MaterialModule} from './material.module';
import {StageComponent} from './stage/stage.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    StageComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule
  ],
  providers: [ProcessingRateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
