import {MatButtonModule, MatCardModule, MatToolbarModule, MatGridListModule} from '@angular/material';
import {NgModule} from '@angular/core';

@NgModule({
  imports: [MatButtonModule, MatToolbarModule, MatCardModule, MatGridListModule],
  exports: [MatButtonModule, MatToolbarModule, MatCardModule, MatGridListModule],
})
export class MaterialModule { }
