import {Rate} from '../model/rate';

export class RateQueue {

  private readonly size: number;

  private count: number;

  public arr: Array<Rate>;

  constructor(size: number) {
    this.size = size;
    this.count = 0;
    this.arr = [];
  }

  public push(value: Rate) {
    if (this.count < this.size) {
      this.arr.push(value);
    } else {
      this.arr.splice(0, 1);
      this.arr.push(value);
    }
    this.count++;
  }

}
