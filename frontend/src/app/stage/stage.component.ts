import {Component, Input, OnInit} from '@angular/core';
// import * as Chartist from 'chartist';
import {ProcessingRateService} from '../services/processing-rate.service';
import {Chart} from 'chart.js';
import {RateQueue} from '../util/rate-queue';
import {Observable} from 'rxjs';
import {Rate} from '../model/rate';

@Component({
  selector: 'app-stage',
  templateUrl: './stage.component.html',
  styleUrls: ['./stage.component.css'],
  providers: [ProcessingRateService]
})
export class StageComponent implements OnInit {

  chart = [];

  @Input() stage: number;

  private stage1Rate: RateQueue;

  constructor(private processingRateService: ProcessingRateService) {
    this.stage1Rate = new RateQueue(15);
  }

  ngOnInit() {
    this.processingRateService.getCurrentRateOfStageStream(this.stage).subscribe(
      (stageRate) => {
        this.stage1Rate.push(stageRate);
        const labels = this.stage1Rate.arr.map((rate) => rate.timestamp);
        const itemDataset = this.stage1Rate.arr.map((rate) => rate.processRate);
        const defectDataset = this.stage1Rate.arr.map((rate) => rate.defectivePersentage);
        // console.log(labels);
        // console.log(dataset);
        this.chart = new Chart('canvas' + this.stage, {
          type: 'line',
          data: {
            labels: labels,
            datasets: [
              {
                data: itemDataset,
                borderColor: '#0D47A1',
                backgroundColor: '#0D47A155',
              },
              {
                data: defectDataset,
                borderColor: '#FF0000',
                backgroundColor: '#FF000055',
              },
            ],
          },
          options: {
            tooltips: {
              enabled: false,
            },
            hover: {
              mode: null
            },
            animation: {
              duration: 0,
            },
            legend: {
              display: false
            },
            scales: {
              xAxes: [{
                display: true,
              }],
              yAxes: [{
                display: true,
                ticks: {
                  max: 140,
                  beginAtZero: true,
                }
              }],
            }
          }
        });
      }
    );

  }

}
