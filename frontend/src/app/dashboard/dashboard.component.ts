import {Component, OnInit} from '@angular/core';
// import * as Chartist from 'chartist';
import {ProcessingRateService} from '../services/processing-rate.service';
import {Chart} from 'chart.js';
import {RateQueue} from '../util/rate-queue';
import {Observable} from 'rxjs';
import {Rate} from '../model/rate';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [ProcessingRateService]
})
export class DashboardComponent implements OnInit {

  rate: Observable<number>;
  defects: Observable<number>;

  private stage1Rate: RateQueue;

  constructor(private processingRateService: ProcessingRateService) {
    this.stage1Rate = new RateQueue(15);
  }

  ngOnInit() {

    this.rate = this.processingRateService.getCurrentRateStream();
    this.defects = this.processingRateService.getDefectiveRateStream();

  }

}
