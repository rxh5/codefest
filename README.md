# RxH5 Codefest Round 1

_Realtime production line monitoring system using data from IOT devices._

**Technologies Used**
* Java 8 for the backend
* Spring Boot 5 with WebFlux to have a reactive backend
* Angular 6 for the frontend
* MongoDB as the datasource

**How to run**
* Clone the repository
* Change current directory to the cloned repository
* Start the MongoDB service `$ sudo service mongod start`
* Start the backend
    * Start a new terminal
    * Change current directory to backend `cd backend`
    * Start Spring Boot backend `$ ./gradlew bootRun`
* Start the frontend
    * Start a new terminal
    * Change current directory to frontend `cd frontend`
    * Run Angular frontend `$ ng serve --open`
* Dashboard will be hosted on `localhost:4200`

**Team**

* Udesh Kumarasinghe
* Sashika Nawarathne
* Ilthizam Imtiyaz
* Sumayya Ziyad