package com.rxh5.codefest_round_1.repository;

import com.rxh5.codefest_round_1.model.ItemCollection;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.mongodb.repository.Tailable;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;

@Repository
public interface ItemRepository extends ReactiveMongoRepository<ItemCollection, String> {

    @Tailable
    Flux<ItemCollection> findWithTailableCursorBy();

    @Tailable
    Flux<ItemCollection> findWithTailableCursorByStage(int stage);

    Flux<ItemCollection> findByTimestampBetween(LocalDateTime startAt, LocalDateTime endAt);

    Flux<ItemCollection> findByTimestampBetweenAndStage(LocalDateTime startAt, LocalDateTime endAt, int stage);

}




