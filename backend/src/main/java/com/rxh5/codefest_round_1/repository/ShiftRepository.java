package com.rxh5.codefest_round_1.repository;

import com.rxh5.codefest_round_1.model.Shift;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;

@Repository
public interface ShiftRepository extends ReactiveMongoRepository<Shift, String> {

    Flux<Shift> findByStartAtLessThanEqualAndEndAtGreaterThanEqualAndEmployName(LocalDateTime startAt, LocalDateTime endAt, String employName);

    Flux<Shift> findByStartAtLessThanEqualAndEndAtGreaterThanEqualAndManagerName(LocalDateTime startAt, LocalDateTime endAt, String managerName);

    Flux<Shift> findByStartAtLessThanEqualAndEndAtGreaterThanEqualAndEmployNameAndManagerName(LocalDateTime startAt, LocalDateTime endAt, String employName, String managerName);

}