package com.rxh5.codefest_round_1;

import com.rxh5.codefest_round_1.model.Item;
import com.rxh5.codefest_round_1.model.ItemCollectionRequest;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@SpringBootApplication
public class CodefestRound1Application {

    @Bean
    WebClient webClient() {
        return WebClient.create("http://localhost:8080");
    }

    @Bean
    CommandLineRunner commandLineRunner(WebClient webClient) {
        return strings -> {
            emulateIOTDevice(webClient, 1);
            emulateIOTDevice(webClient, 2);
            emulateIOTDevice(webClient, 3);
            emulateIOTDevice(webClient, 4);
            emulateIOTDevice(webClient, 5);
        };
    }

    public static void main(String[] args) {
        SpringApplication.run(CodefestRound1Application.class, args);
    }

    private void emulateIOTDevice(WebClient webClient, int stage) {
        Random random = new Random();

        Flux.interval(Duration.ofSeconds(1)).flatMap(
                (Long index) -> {
                    int randomNum = ThreadLocalRandom.current().nextInt(90, 121);
                    Item[] items = new Item[randomNum];

                    for (int i = 0; i < randomNum; i++) {
                        items[i] = new Item(random.nextBoolean());
                    }

                    ItemCollectionRequest request = new ItemCollectionRequest(randomNum, items, stage, LocalDateTime.now());
//                    System.out.println(request.getNoOfItems());

                    return webClient.post().uri("/items")
                            .contentType(MediaType.APPLICATION_JSON_UTF8)
                            .accept(MediaType.APPLICATION_JSON_UTF8)
                            .body(Mono.just(request), ItemCollectionRequest.class)
                            .retrieve()
                            .bodyToMono(ItemCollectionRequest.class);
                }
        ).subscribe();
    }

}
