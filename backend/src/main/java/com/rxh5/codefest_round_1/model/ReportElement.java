package com.rxh5.codefest_round_1.model;

public class ReportElement {

    private String label;

    private int noOfItems;

    private int noOfDefects;

    public ReportElement() {}

    public ReportElement(String label, int noOfItems, int noOfDefects) {
        this.label = label;
        this.noOfItems = noOfItems;
        this.noOfDefects = noOfDefects;
    }

    // Getters
    public String getLabel() {
        return label;
    }

    public int getNoOfItems() {
        return noOfItems;
    }

    public int getNoOfDefects() {
        return noOfDefects;
    }

    // Setters
    public void setLabel(String label) {
        this.label = label;
    }

    public void setNoOfItems(int noOfItems) {
        this.noOfItems = noOfItems;
    }

    public void setNoOfDefects(int noOfDefects) {
        this.noOfDefects = noOfDefects;
    }
}
