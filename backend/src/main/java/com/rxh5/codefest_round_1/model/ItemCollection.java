package com.rxh5.codefest_round_1.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Date;

@Document(collection = "item")
public class ItemCollection extends BaseItemCollection {

    @Id
    private String id;

    @NotNull
    private int noOfDefected = 0;

    public ItemCollection() {

    }

    public ItemCollection(int noOfItems, int noOfDefected, int stage, LocalDateTime timestamp) {
        super(noOfItems, stage, timestamp);
        this.noOfDefected = noOfDefected;
    }

    // Getters
    public String getId() {
        return id;
    }

    public int getNoOfDefected() {
        return noOfDefected;
    }

    // Setters
    public void setId(String id) {
        this.id = id;
    }

    public void setNoOfDefected(int noOfDefected) {
        this.noOfDefected = noOfDefected;
    }
}
