package com.rxh5.codefest_round_1.model;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public class BaseItemCollection {

    @NotNull
    protected int noOfItems = 0;

    @NotNull
    protected int stage = -1;

    @NotNull
    protected LocalDateTime timestamp;

    public BaseItemCollection() {

    }

    public BaseItemCollection(int noOfItems, int stage, LocalDateTime timestamp) {
        this.noOfItems = noOfItems;
        this.stage = stage;
        this.timestamp = timestamp;
    }

    // Getters
    public int getNoOfItems() {
        return noOfItems;
    }

    public int getStage() {
        return stage;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    // Setters
    public void setNoOfItems(int noOfItems) {
        this.noOfItems = noOfItems;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }
}
