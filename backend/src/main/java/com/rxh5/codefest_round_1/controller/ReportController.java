package com.rxh5.codefest_round_1.controller;

import com.rxh5.codefest_round_1.model.BaseItemCollection;
import com.rxh5.codefest_round_1.model.ItemCollection;
import com.rxh5.codefest_round_1.model.ReportElement;
import com.rxh5.codefest_round_1.model.Shift;
import com.rxh5.codefest_round_1.repository.ItemRepository;
import com.rxh5.codefest_round_1.repository.ShiftRepository;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.List;

@RestController
public class ReportController {

    private final ItemRepository itemRepository;
    private final ShiftRepository shiftRepository;

    @Autowired
    ReportController(ItemRepository itemRepository, ShiftRepository shiftRepository) {
        this.itemRepository = itemRepository;
        this.shiftRepository = shiftRepository;
    }

    @CrossOrigin
    @GetMapping(value = "/report")
    Mono<List<ReportElement>> getReport(@RequestParam(value = "startAt") LocalDateTime startAt,
                                        @RequestParam(value = "endAt") LocalDateTime endAt,
                                        @RequestParam(value = "period") String period,
                                        @RequestParam(value = "stage", required = false) int stage,
                                        @RequestParam(value = "employ", required = false) String employ,
                                        @RequestParam(value = "manager", required = false) String manager) {
        List<Pair<LocalDateTime, LocalDateTime>> periods = getPeriods(startAt, endAt, period);

        Flux<Shift> shift;
        if (employ != null && manager != null) {
            shift = shiftRepository.findByStartAtLessThanEqualAndEndAtGreaterThanEqualAndEmployNameAndManagerName(startAt, endAt, employ, manager);
        } else if (employ != null) {
            shift = shiftRepository.findByStartAtLessThanEqualAndEndAtGreaterThanEqualAndEmployName(startAt, endAt, employ);
        } else if (manager != null) {
            shift = shiftRepository.findByStartAtLessThanEqualAndEndAtGreaterThanEqualAndManagerName(startAt, endAt, manager);
        }

        return Flux.fromIterable(periods)
                .flatMap((timePeriod) -> {
                    Flux<ItemCollection> itemCollection;
                    if (stage == 0) {
                        itemCollection = itemRepository.findByTimestampBetween(timePeriod.getValue(), timePeriod.getKey());
                    } else {
                        itemCollection = itemRepository.findByTimestampBetweenAndStage(timePeriod.getValue(), timePeriod.getKey(), stage);
                    }
                    return itemCollection.collectList().map((List<ItemCollection> items) -> {
                        int count = items.stream().mapToInt(ItemCollection::getNoOfItems).sum();
                        int defects = items.stream().mapToInt(ItemCollection::getNoOfDefected).sum();
                        return new ReportElement(timePeriod.getValue().toString(), count, defects);
                    });
                }).collectList();
    }

    private List<Pair<LocalDateTime, LocalDateTime>> getPeriods(LocalDateTime startAt, LocalDateTime endAt, String
            period) {
        List<Pair<LocalDateTime, LocalDateTime>> periods = new ArrayList<>();

        if (period.equalsIgnoreCase("year")) {
            long years = ChronoUnit.YEARS.between(startAt, endAt);

            if (years == 0) {
                periods.add(new Pair<>(endAt, startAt));
            } else if (years > 0) {
                LocalDateTime date = endAt;
                do {
                    date = date.minusYears(1);
                    periods.add(new Pair<>(endAt, date));
                } while (date.isAfter(startAt));
                if (startAt.isBefore(date.plusYears(1)))
                    periods.add(new Pair<>(date.plusYears(1), startAt));
            }

        } else if (period.equalsIgnoreCase("month")) {
            long months = ChronoUnit.MONTHS.between(startAt, endAt);

            if (months == 0) {
                periods.add(new Pair<>(endAt, startAt));
            } else if (months > 0) {
                LocalDateTime date = endAt;
                do {
                    date = date.minusMonths(1);
                    periods.add(new Pair<>(endAt, date));
                } while (date.isAfter(startAt));
                if (startAt.isBefore(date.plusMonths(1)))
                    periods.add(new Pair<>(date.plusMonths(1), startAt));
            }

        } else if (period.equalsIgnoreCase("week")) {
            long weeks = ChronoUnit.WEEKS.between(startAt, endAt);

            if (weeks == 0) {
                periods.add(new Pair<>(endAt, startAt));
            } else if (weeks > 0) {
                LocalDateTime date = endAt;
                do {
                    date = date.minusWeeks(1);
                    periods.add(new Pair<>(endAt, date));
                } while (date.isAfter(startAt));
                if (startAt.isBefore(date.plusWeeks(1)))
                    periods.add(new Pair<>(date.plusWeeks(1), startAt));
            }
        } else if (period.equalsIgnoreCase("day")) {
            long days = ChronoUnit.DAYS.between(startAt, endAt);

            if (days == 0) {
                periods.add(new Pair<>(endAt, startAt));
            } else if (days > 0) {
                LocalDateTime date = endAt;
                do {
                    date = date.minusDays(1);
                    periods.add(new Pair<>(endAt, date));
                } while (date.isAfter(startAt));
                if (startAt.isBefore(date.plusDays(1)))
                    periods.add(new Pair<>(date.plusDays(1), startAt));
            }
        } else if (period.equalsIgnoreCase("hour")) {
            long hours = ChronoUnit.HOURS.between(startAt, endAt);

            if (hours == 0) {
                periods.add(new Pair<>(endAt, startAt));
            } else if (hours > 0) {
                LocalDateTime date = endAt;
                do {
                    date = date.minusHours(1);
                    periods.add(new Pair<>(endAt, date));
                } while (date.isAfter(startAt));
                if (startAt.isBefore(date.plusHours(1)))
                    periods.add(new Pair<>(date.plusHours(1), startAt));
            }
        }
        return periods;
    }

}
