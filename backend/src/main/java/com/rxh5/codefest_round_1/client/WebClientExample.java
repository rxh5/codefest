package com.rxh5.codefest_round_1.client;

import com.rxh5.codefest_round_1.model.ItemCollectionRequest;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

public class WebClientExample {
    public static void main(String[] args) {
        System.out.println("Sex");
        WebClient webClient = WebClient.create("http://localhost:8080");
        Mono<ItemCollectionRequest> result = webClient.get().uri("/items")
                .retrieve()
                .bodyToMono(ItemCollectionRequest.class);
        result.subscribe((response) -> {
            System.out.println(response.getNoOfItems());
        });
    }
}