package com.rxh5.codefest_round_1;


import com.rxh5.codefest_round_1.model.Item;
import com.rxh5.codefest_round_1.model.ItemCollectionRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WebfluxDemoApplicationTests {

    private WebTestClient webTestClient = WebTestClient.bindToServer().baseUrl("http://localhost:8080").build();

    @Test
    public void testIOTDevice2() {
        Random random = new Random();

        Item[] items = new Item[50];

//        Flux.interval(Duration.ofSeconds(1)).subscribe((Long index) -> {
            for (int i = 0; i < 50; i++) {
                items[i] = new Item(random.nextBoolean());
            }

            ItemCollectionRequest request = new ItemCollectionRequest(50, items, 1, LocalDateTime.now());

            webTestClient.post().uri("/items")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .accept(MediaType.APPLICATION_JSON_UTF8)
                    .body(Mono.just(request), ItemCollectionRequest.class)
                    .exchange()
                    .expectStatus().isOk()
                    .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                    .expectBody()
                    .jsonPath("$.id").isNotEmpty();
//        });

    }

}